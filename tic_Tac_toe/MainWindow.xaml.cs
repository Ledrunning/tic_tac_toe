﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace tic_Tac_toe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage bmpImgCross;
        BitmapImage bmpImgZero;
        Boolean XMovement = false;
        MediaPlayer player = new MediaPlayer();

       

        // Состояние клеток в массиве
        CellState[] cellStates = new CellState[9]
            {
                CellState.NotSelected, CellState.NotSelected, CellState.NotSelected,
                CellState.NotSelected, CellState.NotSelected, CellState.NotSelected,
                CellState.NotSelected, CellState.NotSelected, CellState.NotSelected
            };

        public MainWindow()
        {
            InitializeComponent();
           
            // Подгружаю картинки в *.png
            Uri uriImgZero = new Uri(@"pack://application:,,,/Resources/Zero.png", UriKind.Absolute);
            Uri uriImgCross = new Uri(@"pack://application:,,,/Resources/Cross.png", UriKind.Absolute);
            bmpImgZero = GetBitmapImage(uriImgZero);
            bmpImgCross = GetBitmapImage(uriImgCross);

            // Подключаю музыку
            player.MediaFailed += (s, e) => MessageBox.Show("Error");
            player.Open(new Uri("../../Resources/Cannon_Fodder.mp3", UriKind.RelativeOrAbsolute));
            player.Play();

            // Обработчик кнопок
            for (int i = 0; i < LayoutRoot.Children.Count; i++)
            {
                if (LayoutRoot.Children[i] is Button)
                {
                    Button btn = (Button)LayoutRoot.Children[i];
                    btn.Click += new RoutedEventHandler(Button_Click);
                }
            }

             
        }

        // Отключаю музыку
        void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            player.Close();
        }

        BitmapImage GetBitmapImage(Uri uri)
        {
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = uri;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Image img = (Image)btn.Content;
            int btnNumber = Convert.ToInt32((string)btn.Tag);

            if (XMovement && cellStates[btnNumber] == CellState.NotSelected)
            {
                img.Source = bmpImgCross;
                cellStates[btnNumber] = CellState.X;
                XMovement = false;

            }
            else if (cellStates[btnNumber] == CellState.NotSelected)
            {
                img.Source = bmpImgZero;
                cellStates[btnNumber] = CellState.O;
                XMovement = true;
            }

            switch (GetGameResult())
            {
                case GameResult.CrossWin:
                    MessageBox.Show(this, "Crosses wins!\n Sound from Cannon Fodder game");
                    ResetGame();
                    break;
                case GameResult.ZeroWin:
                    MessageBox.Show(this, "Zeroes wins!\n Sound from Cannon Fodder game");
                    ResetGame();
                    break;
                case GameResult.Draw: 
                    MessageBox.Show(this, "Draw!\n Sound from Cannon Fodder game");
                    ResetGame();
                    break;
                case GameResult.ContinueToPlay:
                    break;
            }
        }

        private void ResetGame()
        {
            for (int i = 0; i < cellStates.Length; i++)
                cellStates[i] = CellState.NotSelected;
            for (int i = 0; i < LayoutRoot.Children.Count; i++)
            {
                if (LayoutRoot.Children[i] is Button)
                {
                    Button btn = (Button)LayoutRoot.Children[i];
                    ((Image)btn.Content).Source = null;
                }
            }
        }

        private GameResult GetGameResult()
        {
            // Проверить каждый элемент массива cellStates не равен ли он CellState.NotSelected 
            // Если все неравны, то ничья
            if (cellStates.All<CellState>(c => c != CellState.NotSelected)) 
                return GameResult.Draw;
            if (CheckCellState(CellState.O))
                return GameResult.ZeroWin;
            if (CheckCellState(CellState.X))
                return GameResult.CrossWin;
            return GameResult.ContinueToPlay;
        }

        // Перебираем все варианты совпадений
        private Boolean CheckCellState(CellState cellState)
        {
            if (cellStates[0] == cellState && cellStates[1] == cellState && cellStates[2] == cellState)
                return true;
            else
                if (cellStates[3] == cellState && cellStates[4] == cellState && cellStates[5] == cellState)
                    return true;
                else
                    if (cellStates[6] == cellState && cellStates[7] == cellState && cellStates[8] == cellState)
                        return true;
                    else
                        if (cellStates[0] == cellState && cellStates[3] == cellState && cellStates[6] == cellState)
                            return true;
                        else
                            if (cellStates[1] == cellState && cellStates[4] == cellState && cellStates[7] == cellState)
                                return true;
                            else
                                if (cellStates[2] == cellState && cellStates[5] == cellState && cellStates[8] == cellState)
                                    return true;
                                else
                                    if (cellStates[0] == cellState && cellStates[4] == cellState && cellStates[8] == cellState)
                                        return true;
                                    else
                                        if (cellStates[2] == cellState && cellStates[4] == cellState && cellStates[6] == cellState)
                                            return true;
            return false;
        }
    }
}







